MODULE WPmod
        IMPLICIT NONE
        INTEGER, PARAMETER      :: dp = kind(0.d0)          !double precision
        INTEGER, PARAMETER      :: sp = kind(0.0)           !singel precision
        INTEGER, PARAMETER      :: wp = dp                  !working precision
        REAL(dp), PARAMETER     :: pi_dp = 3.14159265358979323846_dp
        REAL(sp), PARAMETER     :: pi_sp = 3.14159265358979323846_sp
        INTEGER                 :: i, j                     !Iterators

END MODULE
