! Module containing functions used in the pre-exercise for TFY4235.
! Written 12.01.23
! Author: Jonas Frafjord
MODULE functionmod
        USE WPmod
        IMPLICIT NONE
CONTAINS
        FUNCTION fun_single1(x)
            REAL(SP)                :: fun_single1
            REAL(SP), INTENT(IN)    :: x
            fun_single1 = COS(pi_sp/2._SP)/x
        END FUNCTION
        FUNCTION fun_single2(x)
            REAL(SP)                :: fun_single2
            REAL(SP), INTENT(IN)    :: x
            fun_single2 = (1._SP-COS(x))/SIN(x)
        END FUNCTION
        FUNCTION fun_single3(x)
            REAL(SP)                :: fun_single3
            REAL(SP), INTENT(IN)    :: x
            fun_single3 = SIN(x)/(1._SP+COS(x))
        END FUNCTION

        FUNCTION fun_double1(x)
            REAL(DP)                :: fun_double1
            REAL(DP), INTENT(IN)    :: x
            fun_double1 = DCOS(pi_dp/2._DP)/x
        END FUNCTION
        FUNCTION fun_double2(x)
            REAL(DP)                :: fun_double2
            REAL(DP), INTENT(IN)    :: x
            fun_double2 = (1._DP-DCOS(x))/DSIN(x)
        END FUNCTION
        FUNCTION fun_double3(x)
            REAL(DP)                :: fun_double3
            REAL(DP), INTENT(IN)    :: x
            fun_double3 = DSIN(x)/(1._DP+DCOS(x))
        END FUNCTION

END MODULE
