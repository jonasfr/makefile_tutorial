! Module used to initiate seeds for PRNG using a single digit or full entropy
! Written 12.01.23
! Author: Jonas Frafjord

MODULE RANDOM_mod
        USE iso_fortran_env, ONLY: INT64
        IMPLICIT NONE
CONTAINS
        FUNCTION lcg(s)
                INTEGER           :: lcg
                INTEGER(INT64)    :: s
                IF (s == 0) THEN
                        s = 104729
                ELSE
                        s = MOD(s, 4294967296_int64)
                END IF
                s = MOD(s * 279470273_int64, 4294967291_int64)
                lcg = INT(MOD(s, INT(HUGE(0), INT64)), KIND(0))
        END FUNCTION lcg


        SUBROUTINE RANDOM_SINGLE(seed)
                INTEGER(INT64), INTENT(IN)     :: seed
                INTEGER                 :: state_size
                INTEGER, ALLOCATABLE    :: seeds(:)
                INTEGER                 :: i

                CALL RANDOM_SEED(SIZE = state_size)
                ALLOCATE(seeds(state_size))
                DO i = 1,state_size
                        seeds(i) = lcg(seed)
                END DO
                CALL RANDOM_SEED(PUT=seeds)
        END SUBROUTINE



END MODULE
