## Makefile tutorial

## Description
Makefile tutorial for new programmers that want to learn how to structure a project.<br />
Some easy to start makefile examples. Cmake examples are with multiple directories and with multiple cmakefiles working together.

## Installation
Need to install cmake.<br />
For windows user, msys and mingw can be used to run Makefiles and cmake.

## Usage
Clone repository and test makefiles. Use makefiles and cmakefiles as templates for your projects.

## Windows
Install mingw, msys and cmake. In CMD run:<br />
cmake –G “MinGW Makefiles” ..<br />
mingw32-make

Alternatively: use "Visual studio build tools". Prompt include cmake and nmake (version of make).