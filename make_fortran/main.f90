! main file for the pre-exercise for TFY4235
! Written January 2023
! Author: Jonas Frafjord
PROGRAM preexercise
        USE WPmod               ! Contains all parameter values, and WP
        USE functionmod         ! Contains essential functions as subroutines
        IMPLICIT NONE
        REAL(DP)        :: x_double
        REAL(SP)        :: x_single

        PRINT *, ""
        PRINT *, "************** Program has started! *****************"
        PRINT *, "This is a Makefile tutorial and also demonstrates loss of precision"
        PRINT *, "The topic of numbers will be covered in the lectures later, but something to have in mind for now."
        PRINT *, ""
    
        PRINT *, "Single precision can give disastrous results one should be aware of."
        x_single = 1e-9_SP
        x_double = 1e-9_DP
        PRINT *, "Single precision for cos(pi/2)/x, x = 1e-9: ", fun_single1(x_single)
        PRINT *, "Double precision for cos(pi/2)/x, x = 1e-9: ", fun_double1(x_double)
        PRINT *, ""
        
        PRINT *, "************** Part 2 *****************"
        PRINT *, "Both the functions below are analytically equivalent."
        PRINT *, "With x = 1e-4, it should approximate 5.0000000041633333361e-5"

        x_single = 1e-4_SP
        x_double = 1e-4_DP
        PRINT *, "Single precision for (1-cos(x)/sin(x), x = 1e-4: ", fun_single2(x_single)
        PRINT *, "Double precision for (1-cos(x)/sin(x) x = 1e-4: ", fun_double2(x_double)
        PRINT *, "Single precision for sin(x)/(1+cos(x)), x = 1e-4: ", fun_single3(x_single)
        PRINT *, "Double precision for sin(x)/(1+cos(x)), x = 1e-4: ", fun_double3(x_double)
        PRINT *, ""
        PRINT *, ""
        PRINT *, "Notes: Make a workingPrecision (WP) variable and always specify floating numbers with _WP."
        PRINT *, "Trigonometric functions have both single and double precision versions given by e.g. SIN or DSIN."
        PRINT *, "I learned C++ before learning about floating number arithmetics, so I don't know syntax for c++"
        PRINT *, "This will be explained in a lecture at some point during the course."
    
END PROGRAM
